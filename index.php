<?php
function checkInternetConnection() {
    $content = @file_get_contents("http://www.google.com");
    // Essaye de récupérer le contenu de la page Google
    if ($content !== false) {
        return true;
    } else {
        return false; 
    }
}

if (checkInternetConnection()) {
    ?>

    <!DOCTYPE html>
    <html lang="fr">
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Formulaire de Commentaire Anonyme</title>
      <link rel="stylesheet" href="styles.css">
    </head>
    <body>

      <div class="overlay" onclick="closeModal()"></div>

      <div class="modal">
        <div class="close" onclick="closeModal()">&times;</div>
        <div class="avatar">
          <img src="./img/user.jpg">
        </div>
        <form id="commentForm" method="POST" class="modal-content">
          <input type="text" id="message" class="message" name="message" placeholder="Message...">
          <p class="add" onclick="addProfanity()">Ajouter un mot</p>
          <input type="submit" name="ok" class="send" value="Envoyer">
        </form>
      </div>

      <button onclick="openModal()" class="btn">⭐</button>

      <script>
        function openModal() {
          document.querySelector('.modal').classList.add('show');
          document.querySelector('.overlay').classList.add('show');
        }

        function closeModal() {
          document.querySelector('.modal').classList.remove('show');
          document.querySelector('.overlay').classList.remove('show');
          document.querySelector('.close').classList.remove('show');
        }

        function addProfanity() {
          var profanity = prompt("Entrez un gros mot:");
          var list = document.getElementById("profanityList");
          var listItem = document.createElement("li");
          listItem.textContent = profanity;
          list.appendChild(listItem);
        }
      </script>

      <?php
      function containsProfanity($message)
      {
        $profanityList = array("fait chier", "ton gros cul", "merde", "salot", "vas te faire foutre", "fils de pute", "fuck boy", "pussy", "baltringe", "shetanne");
        foreach ($profanityList as $profanity) {
          if (stripos($message, $profanity) !== false) {
            return true;
          }
        }
        return false;
      }

      function callOpenAI($message)
      {
        $openai_endpoint = "https://api.openai.com/v1/chat/completions";
        $openai_token = "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";

        if (containsProfanity($message)) {
          return "Ce mot est inapproprié. Veuillez reformuler.";
        }

        $data = array(
          "model" => "gpt-3.5-turbo",
          "messages" => array(
            array(
              "role" => "system",
              "content" => "Vous parlez avec ChatGPT"
            ),
            array(
              "role" => "user",
              "content" => $message,
            )
          ),
          "max_tokens" => 100,
          "temperature" => 0.7,
        );

        $headers = array(
          "Content-Type: application/json",
          "Authorization: Bearer " . $openai_token
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $openai_endpoint);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
      }

      if (isset($_POST['message']) && isset($_POST['ok'])) {
        $message = $_POST['message'];
        if (containsProfanity($message)) {
          echo "<p>Message contient des mots grossiers. Veuillez reformuler.</p>";
        } else {
          $response = callOpenAI($message);
          echo "<p>".$response."</p>";
        }
      }
      ?>
    </body>
    </html>

    <?php
} else {
    
    header("HTTP/1.0 404 Not Found");
    include("404.html");
}
?>
